<?php
//use PHPUnit\Framework\TestCase;

//class NumberTest extends TestCase

class NumberTest extends PHPUnit_Framework_TestCase
{
    public function testfun1()
    {
        // Arrange
        $target = new \App\Number(1);
        $excepted = 1;

        // Act
        $actual = $target->get();

        // Assert
        $this->assertEquals($excepted, $actual);
    }
}
